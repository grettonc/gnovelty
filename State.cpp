/************************************************************/
/* gnovelty+.hpp, version 1.2                               */
/*                                                          */
/*      A greedy gradient based Novelty+                    */
/*                                                          */
/*      1. Duc Nghia Pham (duc-nghia.pham@nicta.com.au)	    */ 
/*            National ICT Australia Ltd.                   */
/*            IIIS, Griffith University, Australia          */
/*                                                          */
/*      2. Charles Gretton (charles.gretton@computer.org)   */ 
/*            National ICT Australia Ltd. 2006/09; 2011/12  */
/*            University of Birmingham (2009-2011)          */
/*            IIIS, Griffith University, Australia 2006/12  */
/*                                                          */
/*                                                          */
/************************************************************/

#include "State.hpp"


using namespace tr1;

Basic_State& Basic_State::operator=(const Basic_State& state)
{
    // std::cerr<<"WARNING::State copying is forbidden, exiting."<<std::endl;
    // assert(0);
    // exit(0);
    VERBOSER(1, "Basic_State::operator="<<endl);

    /*Assert we are not copy constructing a null-state.*/
    assert(state.numPropositions);
    
    this->numPropositions = state.numPropositions;
    //this->data = state.data;

    if(0 != data){
        VERBOSER(1, "Deleting old bitvector."<<endl);
        delete [] data;
    }

    VERBOSER(1, "Allocating new bitvector."<<endl);
    data = new ELEM_TYPE[CEIL(numPropositions)];
    for(uint i = 0; i < CEIL(numPropositions); i++){    
        data[i] = state.data[i];
    }
    
    assert(*this == state);
    
    return *this;
}

Basic_State::Basic_State(const Basic_State& state)
    :numPropositions(state.numPropositions)
{
    //exit(0);
    VERBOSER(1, "Basic_State::Basic_State() Copy construction"<<endl);
    
    data = new ELEM_TYPE[CEIL(numPropositions)];
    for(uint i = 0; i < CEIL(numPropositions); i ++)data[i] = state.data[i];

    assert(*this == state);
    
}

Basic_State::Basic_State(uint size)
    :numPropositions(size),
     data(0)
{
    //data = vector<lui>(CEIL(size));
    //data = new lui[CEIL(size)];
    
//     for(uint i =0; i < data.size(); i++){
// 	data[i] = 0;
//     }

//     for(uint i = SIZE_ELEM * data.size(); i > numPropositions; i--){
// 	flipOn(i);
//     }
    assert(size == numPropositions);
    data = new ELEM_TYPE[CEIL(numPropositions)];
    
    for(uint i = 0; i < CEIL(numPropositions); i++){
        VERBOSER(2, "Assignment to element "<<i<<" in data of size "<<(CEIL(numPropositions))<<"\n");
	data[i] = 0;
        VERBOSER(2, "Done assignment to element "<<i<<" in data\n");
    }
    
//     for(uint i = (SIZE_ELEM * CEIL(numPropositions)) - 1 ; i > numPropositions; i--){
//         assert(i >= 0);
//         assert(i > numPropositions);
        
//         VERBOSER(2, "Assignment to element "<<i<<" in data of size "<<(SIZE_ELEM * CEIL(numPropositions))<<"\n");
// 	flipOn(i);
//         VERBOSER(2, "*** Assignment to element "<<i<<" in data of size "<<(SIZE_ELEM * CEIL(numPropositions))<<"\n");
//     }
}

Basic_State::~Basic_State()
{
    if(0 != data)
        delete [] data;
}


bool Basic_State::operator==(const Basic_State& state) const
{
    if(numPropositions != state.numPropositions) return false;
    
    for(uint i = 0; i < CEIL(numPropositions); i++){
        if(data[i] != state.data[i]) {
            return false;
        }
    }
    
    return true;//state.data == this->data;
}
 
bool Basic_State::operator<(const Basic_State& state) const
{
    if(numPropositions > state.numPropositions){
        return false;
    } else if (numPropositions == state.numPropositions) {
        for(uint i = 0; i < CEIL(numPropositions); i++){
            if(data[i] > state.data[i]) {
                return false;
            } else if (data[i] < state.data[i]) {
                return true;
            }
        }
    } else {
        return true;
    }
    
    assert(*this == state);
    
    return false;
    
    
//     return (state.data.size() < this->data.size())?true:
// 	((state.data.size() == this->data.size())?(state.data < this->data):false);
}
    
size_t Basic_State::new_hash_if_flip(uint index){
    flip(index);
    size_t result = this->hash_value();
    flip(index);
    return result;
}

size_t Basic_State::hash_value() const
{
    register size_t seed = 0;
    register uint i;
    
    for(i = 0; i < CEIL(numPropositions); i++){
        boost::hash_combine(seed, data[i]);
    }
    
    return seed;

    /*The following did not seem to be thread safe.*/
    
    //return boost::hash_range(data, data + numPropositions);//data.begin(), data.end());
}

void Basic_State::randomize()
{
    register ELEM_TYPE tmp;
    register uint i;
    register unsigned short int j;
	
    for( i = 0; i < numPropositions/*data.size()*/; i++){
	for(j = 0 ; j < sizeof(ELEM_TYPE) * 8; j++){
	    tmp = 1;
	    tmp = tmp<<j;
	    if(random() % 2){
		tmp = tmp^big;
		data[i] &= tmp;
	    } else {
		data[i] |= tmp;
	    }
	}
    }
}

// /*Change individual (at index \argument{uing}) bits of the
//  * state representation (\member{data}).*/
// void Basic_State::flipOn(uint index)
// {
//     uint remainder = REMAINDER(index);
//     uint bitVecNum = FLOOR(index);

//     uint tmp = 1;
//     tmp = tmp<<remainder;
//     data[bitVecNum] |= tmp;
    
//     /*Now it's true, some actions may be executable.*/
// }

// void Basic_State::flipOff(uint index)
// {
//     uint remainder = REMAINDER(index);
//     uint bitVecNum = FLOOR(index);

//     uint tmp = 1;
//     tmp = tmp<<remainder;
//     tmp = tmp^big;
//     data[bitVecNum] &= tmp;
// }

// void Basic_State::flip(uint index)
// {
//     uint remainder = REMAINDER(index);
//     uint bitVecNum = FLOOR(index);

//     uint tmp = 1;
//     tmp = tmp<<remainder;
    
//     if(data[bitVecNum] & tmp){
// 	tmp = tmp^big;
// 	data[bitVecNum] &= tmp;
//     } else {
// 	data[bitVecNum] |= tmp;

// 	/*Now it's true, some actions may be executable.*/
//     }
// }

// bool Basic_State::isTrue(uint index) const
// {
//     uint remainder = REMAINDER(index);
//     uint bitVecNum = FLOOR(index);

//     uint tmp = 1;
//     tmp = tmp<<remainder;
    
//     return (data[bitVecNum] & tmp)?true:false;
// }



    
/*Function for STL and boost to access \member{hash_value} of
 * \argument{GroundAction}.*/
std::size_t hash_value(const Basic_State& state)
{
	return state.hash_value();
}


