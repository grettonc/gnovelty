/************************************************************/
/* gnovelty+.hpp, version 1.2                               */
/*                                                          */
/*      A greedy gradient based Novelty+                    */
/*                                                          */
/*      1. Duc Nghia Pham (duc-nghia.pham@nicta.com.au)	    */ 
/*            National ICT Australia Ltd.                   */
/*            IIIS, Griffith University, Australia          */
/*                                                          */
/*      2. Charles Gretton (charles.gretton@computer.org)   */ 
/*            National ICT Australia Ltd. 2006/09; 2011/12  */
/*            University of Birmingham (2009-2011)          */
/*            IIIS, Griffith University, Australia 2006/12  */
/*                                                          */
/*                                                          */
/************************************************************/
/*    Consult legal.txt for legal information    		  */
/**********************************************************/
/* Partial of the code is based on UBCSAT version 1.0     */
/* written by:                                            */
/* Dave A.D. Tompkins & Holger H. Hoos                    */
/*   Univeristy of British Columbia                       */
/* February 2004                                          */
/**********************************************************/

/************************************/
/* Standard includes                */
/************************************/
#include <iostream>
#include <cerrno>
#include <cstdio>

#include "global.hpp"
#include "gnovelty+.hpp"
#include "rtime.hpp"

/**********************************************************/
/* MACROS                                                 */
/**********************************************************/
#define IS_TRUE(Z) (state.isTrue(Z))

#define getScore(VAR)		(varWeightDiffs[VAR])
#define setScore(VAR, VAL)	(varWeightDiffs[VAR]  = (cw_t) VAL)
#define adjScore(VAR, VAL)	(varWeightDiffs[VAR] += VAL)
#define incScore(VAR, CLS)	(varWeightDiffs[VAR] += clsWeights[CLS])
#define decScore(VAR, CLS)	(varWeightDiffs[VAR] -= clsWeights[CLS])


/**********************************************************/
/* ANOV parameters                                        */
/**********************************************************/
static const
int invPhi = 5;

static const
int invTheta = 6;

/**********************************************************/
/* Parallel parameters                                    */
/**********************************************************/

//vector<GNoveltyPlus*> gthreads;

/** 
 * Once a solution is found by one solver, we must stop
 * other solvers from declaring they found a solution also.
 */
pthread_mutex_t solution_mutex;
pthread_mutex_t running_mutex;
size_t running_count = 0;

void decrement__running_count(){
	while ( pthread_mutex_trylock (&running_mutex) == EBUSY ) sleep(.001);
	--running_count;
    if ( 0 != pthread_mutex_unlock(&running_mutex) ) 
        cerr << "Unable to unlock mutex."<< endl;
}

void increment__running_count(){
	while ( pthread_mutex_trylock (&running_mutex) == EBUSY ) sleep(.001);
	++running_count;
	if ( 0 != pthread_mutex_unlock(&running_mutex) )
		cerr << "Unable to unlock mutex." << endl;
}

/**********************************************************/
/* static functions                                       */
/**********************************************************/
bool no_solution_yet = true;

void* runGNoveltyPlus(void* _gthread) {
	if ( !no_solution_yet ) {
		decrement__running_count();
		pthread_exit(0);
	}
    
	double starttime;
	double timelimit;
    
	GNoveltyPlus& gthread = *reinterpret_cast<GNoveltyPlus*>(_gthread);
	VERBOSER(1, "Made it into the search thread.\n");
	
	starttime = elapsedTime();
	timelimit = starttime + timeout;
	
	gthread.init();
	
//	while ( gthread.falseClauses[0] > target ) {
	while ( no_solution_yet && gthread.falseClauses[0] > target && elapsedTime() < timelimit && 
		((double) gthread.setOfStates.size() / (double) gthread.setOfStates.maximum_size() < .8)) {

            //std::cerr<<gthread.setOfStates.size()<<" "<<gthread.setOfStates.maximum_size()<<std::endl;

		gthread.flip++;
		
		gthread.var = gthread.pickVar();
		
		gthread.flipVar(gthread.var);
		
                if(gthread.var >= 1){
                    if(!(gthread.var >=1 && gthread.var < nbVars + 1)){
                        cerr<<gthread.var<<std::endl;;
                    }
                    assert(gthread.var >=1 && gthread.var < nbVars + 1);

                    gthread.varAges[gthread.var] = gthread.flip;
                }
		
		if ( gthread.adaptive )
			gthread.adaptNoveltyNoise();
		
		gthread.updateStatsEndFlip();
	}

	gthread.runtime = elapsedTime() - starttime;
	gthread.updateStatsEndTry();
        
	decrement__running_count();
	pthread_exit(0);
	
	// No need to use the return value of the thread.
	return 0;
}

void findNeighbours() {
    //int *_neighbours = new int[nbVars + 1];
	int *_neighbours = new int[nbVars + 2];
	std::fill(_neighbours, _neighbours + nbVars + 2, 0);
	
	neighbours = new int*[nbVars + 1];
	for ( int i = 1; i <= nbVars; ++i ) {
//		std::cerr << "n(" << i << ") :";
		int nbNeighbours = 0;
		
		for ( int *optr = occurs[nbVars - i] + 1; *optr != NONE; ++optr ) {
			int c = *optr;
			for ( int j = 1; j <= clauses[c][0]; ++j ) {
				int v = Var( c, j );
				if ( v == i || _neighbours[v] == i ) continue;
				_neighbours[v] = i;
				++nbNeighbours;
			}
		}
		
		for ( int *optr = occurs[nbVars + i] + 1; *optr != NONE; ++optr ) {
			int c = *optr;
			for ( int j = 1; j <= clauses[c][0]; ++j ) {
				int v = Var( c, j );
				if ( v == i || _neighbours[v] == i ) continue;
				_neighbours[v] = i;
				++nbNeighbours;
			}
		}
		
		neighbours[i] = new int[nbNeighbours + 1];
		int *nptr = neighbours[i];
		for ( int j = 1; j <= nbVars; ++j ) {
			if ( _neighbours[j] != i ) continue;
//			std::cerr << j << " ";
			
			*(nptr++) = j;
			if ( --nbNeighbours == 0 ) break;
		}
		*nptr = NONE;
//		std::cerr << std::endl;
	}
	delete[] _neighbours;
}


/**********************************************************/
/* GNoveltyPlus::functions                                */
/**********************************************************/

int GNoveltyPlus::pickVar(void) {
    cw_t score, bestScore, secondBestScore;
    int  var, bestVar, secondBestVar, youngest, lastChange;

	bestVar   = secondBestVar   = NONE;

    if ( candVars[0] > 0 ) {
        bestScore  = BIG;
        lastChange = flip;

        int i = NONE;
        int *cptr = candVars + 1;
        for ( int j = 1; j <= candVars[0]; ++j, ++cptr ) {
            int var = *cptr;
            score = getScore(var); 

            if ( score < 0 ) {
#ifdef USE_setOfStates
                
                size_t new_hash_if_flip = state.new_hash_if_flip(var);
                if ( setOfStates.find(new_hash_if_flip) != setOfStates.end() ) {
                    i = j;		
                    candidates[var] = false;
                    break;
                }
#endif
				
                if ( score < bestScore ) {
                    bestScore  = score;
                    lastChange = varAges[var];
                    bestVar    = var;
                } else if ( score == bestScore && varAges[var] < lastChange ) {
                    lastChange = varAges[var];
                    bestVar    = var;
                }
            } else {
                i = j;		candidates[var] = false;
                break;
            }
        }
        ++cptr;

        if ( i != NONE ) {
            for ( int j = i + 1; j <= candVars[0]; ++j, ++cptr) {
                var = *cptr;

                score = getScore(var); 
				
                if ( score < 0 ) {
#ifdef USE_setOfStates
                    size_t new_hash_if_flip = state.new_hash_if_flip(var);
                    if ( setOfStates.find(new_hash_if_flip) != setOfStates.end() ) {
                        //std::cerr<<"Skipping"<<std::endl;
                        i = j;		
                        candidates[var] = false;
                        break;
                    }
#endif
					
                    candVars[i++] = var;
                    if ( score < bestScore ) {
                        bestScore  = score;
                        lastChange = varAges[var];
                        bestVar    = var;
                    } else if ( score == bestScore && varAges[var] < lastChange ) {
                        lastChange = varAges[var];
                        bestVar    = var;
                    }
                } else {
                    candidates[var] = false;
                }
            }
            candVars[0] = i - 1;
        }
    }

    if ( random() % 100 < walkProb) {
        int c   = falseClauses[ (random() % falseClauses[0]) + 1 ];
        bestVar = Var( c, (random() % clauses[c][0]) + 1 );
    } else if ( candVars[0] <= 0 ) {
        updateClauseWeights();
        
        bestScore = secondBestScore = BIG;
        bestVar   = secondBestVar   = NONE;

        int c     = falseClauses[ (random() % falseClauses[0]) + 1 ];
        youngest  = varAges[ Var(c, 1) ];

        for ( int j = 1; j <= clauses[c][0]; ++j ) {
            var        = Var(c, j);
            score      = getScore(var);
            lastChange = varAges[var];

            if (lastChange > youngest) youngest = varAges[var];

            if ( (score < bestScore) || ((score == bestScore) && (lastChange < varAges[bestVar])) ) {
                secondBestVar   = bestVar;
                secondBestScore = bestScore;

                bestVar   = var;
                bestScore = score;
            } else if ( (score < secondBestScore) || ((score == secondBestScore) && (lastChange < varAges[secondBestVar])) ) {
                secondBestVar   = var;
                secondBestScore = score;
            }
        }

        if ( varAges[bestVar] == youngest ) {
            if ( (getScore(bestVar) == 0) || (random()%100 < noise) )
                bestVar = secondBestVar;
        }
    }

    return bestVar;
}

void GNoveltyPlus::updateClauseWeights() {
    if ( smoothProb == 100 ) return;

    for ( int i = 1; i <= falseClauses[0]; ++i ) {
        int c = falseClauses[i];
		
        if ( ++clsWeights[c] == 2 ) 
            addWeightedClause(c);
        for ( int j = 1; j <= clauses[c][0]; ++j ) {
            var = Var(c, j);
            adjScore(var, -1);
            if ( !candidates[var] && (getScore(var) < 0) && (varAges[var] < flip) ) {
                candVars[ ++candVars[0] ] = var;
                candidates[var] = true;
            } 
        }
    }
	
    if ( random()%1000 < smoothProb*10 ) smooth();
}

void GNoveltyPlus::smooth() {
    for ( int i = 1; i <= wgtClauses[0]; ++i ) {
        int c = wgtClauses[i];
        if ( --clsWeights[c] == 1 ) {
            remWeightedClause(c);
            --i;
        }
        if ( clsTrueLits[c] == 0 )
            for ( int j = 1; j <= clauses[c][0]; ++j ) adjScore( Var(c, j), 1 );
        else if ( clsTrueLits[c] == 1 ) {
            int var = clsCritVars[c];
            adjScore( var, -1 );
            if ( !candidates[var] && (getScore(var) < 0) && (varAges[var] < flip) ) {
                candVars[ ++candVars[0] ] = var;
                candidates[var] = true;
            } 
        }
    }
}

void GNoveltyPlus::adaptNoveltyNoise() {
    if ( (flip - lastAdaptFlip) > (nbCls / invTheta) ) {
        noise += (int) ((100 - noise) / invPhi);
        lastAdaptFlip     = flip;
        lastAdaptNumFalse = falseClauses[0];
    } else if ( falseClauses[0] < lastAdaptNumFalse ) {
        noise -= (int) (noise / invPhi / 2);
        lastAdaptFlip     = flip;
        lastAdaptNumFalse = falseClauses[0];
    }
}

void GNoveltyPlus::init() {
	VERBOSER(1, "Initiate a state with :: "<<nbVars+1<<" variables."<<endl);
	// The state(nbVars+1) is created when creating the GNoveltyPlus
	
	#ifdef USE_partialState 
		vector<uint> includedInPartialState;
		for ( uint i = 1; i <= nbVars; ++i )
			if ( random() % 2 ) 
				includedInPartialState.push_back(i);
		partialState.operator=(PartialState(includedInPartialState));
	#endif

	VERBOSER(1, "Finish initiating the state."<<endl);

	flip = nullFlip = 0;
	falseClauses[0] = 0;	// the number of false clauses;
	wgtClauses[0]   = 0;	// the number of weighted clauses;

	for ( int i = 0; i < nbCls; ++i ) {
		clsTrueLits[i]  = 0;
		clsWeights[i]   = 1;
	}

	for ( int i = 1; i <= nbVars; ++i ) {
		setScore(i, 0);
		varAges[i] = 0;

        if ( random() % 2 ) {
			state.flipOn(i);
			#ifdef USE_partialState 
				partialState.flipOn__withTrackingTest(i);
			#endif
		} else {
			state.flipOff(i);
			#ifdef USE_partialState 
				partialState.flipOff__withTrackingTest(i);
			#endif
		}
	}

	/**
	 * Hash the new state
	 */
	#ifdef USE_setOfStates
		setOfStates.insert(state);
	#endif
	#ifdef USE_setOfPartialStates
		setOfPartialStates.insert(partialState);
	#endif
        
	/*
	 * Initialise the gradient cost
	 */
	int lit = 0;
	for ( int i = 0; i < nbCls; ++i ) {
		for ( int j = 1; j <= clauses[i][0]; ++j ) {
			lit = Lit(i, j);
			if ( (lit > 0) == IS_TRUE(abs(lit)) ) {
				++clsTrueLits[i];
				clsCritVars[i] = abs(lit);
			}
		}
		if ( clsTrueLits[i] == 0 ) {
			addFalseClause(i);

			//=== these variables make clause i
			for ( int j = 1; j <= clauses[i][0]; ++j )
				decScore( Var(i,j), i );
		} else if ( clsTrueLits[i] == 1 ) {
			//=== this variable breaks clause i
			incScore( clsCritVars[i], i );
		}
	}
	updateStatsEndFlip();

	/*
	 * Initialise the list of candidate variables
	 */
	candVars[0] = 0;	// the number of candidate variables
	for ( int i = 1; i <= nbVars; ++i ) {
		if ( getScore(i) < 0 ) {
			candVars[ ++candVars[0] ] = i;
			candidates[i] = true;
		} else {
			candidates[i] = false;
		}
	}

	// Adapt Novelty+ Noise
	if ( adaptive ) {
		lastAdaptFlip = flip;
		lastAdaptNumFalse = falseClauses[0];
		noise = 0;
	}
}

void GNoveltyPlus::flipVar(int toflip) {
	int var, toEnforce;		//=== literal to enforce

	if ( toflip == NONE ) {
		++nullFlip;
		return;
	}

	if ( IS_TRUE(toflip) ) toEnforce = -toflip;
	else toEnforce = toflip;

	state.flip(toflip);
	#ifdef USE_setOfStates
		setOfStates.insert(state);
                //std::cerr<<setOfStates.size()<<" "<<setOfStates.maximum_size()<<std::endl;
	#endif
	#ifdef USE_partialState
		partialState.flip__withTrackingTest(toflip);
	#endif

	/* Charles-to-Nghia: 
	 * Keep track of the states (and partial states) 
	 * we have already visited, by storing them in a
	 * hash.
	 */
	#ifdef USE_setOfStates
		;
		//setOfStates.insert(state);
	#endif
	#ifdef USE_setOfPartialStates
		setOfPartialStates.insert(partialState);
	#endif
	
	for ( int *nptr = neighbours[toflip]; *nptr != NONE; ++nptr )
		candidates[ *nptr ] = ( getScore(*nptr) < 0 );

	//=== Examine all occurrences of literals with toEnforce with old sign.
	//=== (these literals are made false; clsTrueLits must be decremented in that clause)
	for ( int *optr = occurs[nbVars - toEnforce] + 1; *optr != NONE; ++optr ) {
		int c = *optr;
		if ( --clsTrueLits[c] == 0 ) {
			// clause c is no longer satisfied by lit with toflip; it's now false.
			addFalseClause(c);
			
			//=== Decrement toflip's break.
			decScore(toflip, c);
			//=== Increment make of all vars in clause c.
			for ( int j = 1; j <= clauses[c][0]; ++j )
				decScore( Var(c, j), c );
		} else if ( clsTrueLits[c] == 1 ) {
			//=== Find the lit in this clause that makes it true, and inc its break.
			for ( int j = 1; j <= clauses[c][0]; ++j ) {
				var = Var(c, j);
				if ( (Lit(c, j) > 0) == IS_TRUE(var) ) {
					clsCritVars[c] = var;
					incScore(var, c);
					break;
				}
			}
		}
	}


	//=== Examines all occurrences of literal with toEnforce with new sign 
	//=== (these literals are made true; clsTrueLits must be incremented in that clause)
	for ( int *optr = occurs[nbVars + toEnforce] + 1; *optr != NONE; ++optr ) {
		int c = *optr;
		if ( ++clsTrueLits[c] == 1 ) {
			remFalseClause(c);

			clsCritVars[c] = toflip;
			incScore(toflip, c);
			//=== Decrement the make of all vars in clause c.
			for ( int j = 1; j <= clauses[c][0]; ++j )
				incScore( Var(c, j), c );
		} else if ( clsTrueLits[c] == 2 ) {
			//=== Find the lit in this clause other than toflip that makes it true,
			//=== and decrement its break.
			for ( int j = 1; j <= clauses[c][0]; ++j ) {
				var = Var(c, j);
				if ( (toflip != var) && ((Lit(c,j) > 0) == IS_TRUE(var)) ) {
					decScore(var, c);
					break;
				}
			}
		}
	}

	for ( int *nptr = neighbours[toflip]; *nptr != NONE; ++nptr ) {
		var = *nptr;	
		if ( getScore(var) < 0 && !candidates[var] ) {
			assert( candVars[0] < nbVars + 1 );
			
			candVars[ ++candVars[0] ] = var;
			candidates[var] = true;
		}
	}
}

void GNoveltyPlus::addFalseClause(int c) {
	falseClauses[ ++falseClauses[0] ] = c;
	falseClsIdx[c] = falseClauses[0];
}

void GNoveltyPlus::remFalseClause(int c) {
	int _c = falseClauses[ falseClauses[0] ];
	if ( --falseClauses[0] < falseClsIdx[c] ) return;
	falseClauses[ falseClsIdx[c] ] = _c;
	falseClsIdx[_c] = falseClsIdx[c];
}

void GNoveltyPlus::addWeightedClause(int c) {
	wgtClauses[ ++wgtClauses[0] ] = c;
	wgtClsIdx[c] = wgtClauses[0];
}

void GNoveltyPlus::remWeightedClause(int c) {
	int _c = wgtClauses[ wgtClauses[0] ];
	if ( --wgtClauses[0] < wgtClsIdx[c] ) return;
	wgtClauses[ wgtClsIdx[c] ] = _c;
	wgtClsIdx[_c] = wgtClsIdx[c];
}

void GNoveltyPlus::updateStatsEndTry() {
	totalFlips     += flip;
	totalNullFlips += nullFlip;
	
	while ( pthread_mutex_trylock(&solution_mutex) == EBUSY) sleep(.001);
	if ( no_solution_yet ) {
		finalCost    = falseClauses[0];
		finalFlips   = flip - nullFlip;
		finalRuntime = runtime;

		if ( falseClauses[0] <= target ) {
			no_solution_yet = false;

			saveSolution();
			++successRuns;
			totalSuccessFlips     += flip;
			totalSuccessNullFlips += nullFlip;
		}
	}
	if ( 0 != pthread_mutex_unlock(&solution_mutex) )
		cerr << "Unable to unlock mutex." << endl;
	if ( computeCurrentCost() != falseClauses[0] ) {
		cerr << "Program error, verification of assignment cost fails!" << endl;
		cerr << "Program error :: Currest cost is " << computeCurrentCost() << " and numFalse is " << falseClauses[0] << endl;
		exit(-1);
	}
}

void GNoveltyPlus::updateStatsEndFlip() {
	while ( pthread_mutex_trylock(&solution_mutex) == EBUSY) sleep(.001);
	if ( falseClauses[0] < lowCost ) {
		lowCost  = falseClauses[0];
		lowFlips = flip - nullFlip;
	}
	if ( 0 != pthread_mutex_unlock(&solution_mutex) )
		cerr << "Unable to unlock mutex." << endl;
}

cw_t GNoveltyPlus::computeCurrentCost() {
    cw_t result = 0;

    for ( int i = 0; i < nbCls; ++i ) {
        bool sat = false;
        for ( int j = 1; j <= clauses[i][0]; ++j ) {
            if ( (Lit(i,j) > 0) == IS_TRUE(Var(i, j)) ) {
                sat = true;
                break;
            }
        }
        if (!sat) ++result;
    }
    return result;
}


void GNoveltyPlus::saveSolution() {
    for ( int i = 1; i <= nbVars; ++i ) 
        solution[i] = ( IS_TRUE(i) ) ? true : false;
}

