/************************************************************/
/* gnovelty+.hpp, version 1.2                               */
/*                                                          */
/*      A greedy gradient based Novelty+                    */
/*                                                          */
/*      1. Duc Nghia Pham (duc-nghia.pham@nicta.com.au)	    */ 
/*            National ICT Australia Ltd.                   */
/*            IIIS, Griffith University, Australia          */
/*                                                          */
/*      2. Charles Gretton (charles.gretton@computer.org)   */ 
/*            National ICT Australia Ltd. 2006/09; 2011/12  */
/*            University of Birmingham (2009-2011)          */
/*            IIIS, Griffith University, Australia 2006/12  */
/*                                                          */
/*                                                          */
/************************************************************/

#ifndef __MY_GLOBAL_HPP__
#define __MY_GLOBAL_HPP__


//=== If the constant NT is set the program compiles under Windows NT.
//=== Currently, the timing then reflects user time.
#ifdef WIN32
	#define NT 1
#endif

#ifdef WIN32
	#define random() rand()
	#define srandom(seed) srand(seed)
#endif

/************************************/
/* Compilation flags                */
/************************************/
#define USE_setOfStates 1

// Comment this out if you don't want to hash partial states.
//#define USE_setOfPartialStates 1


/*---------------------------------
 
 Old fashioned debugging
 
 ---------------------------------*/
#define DEBUG_LEVEL 3
#define DEBUG_GET_CHAR(Y) {if(Y > DEBUG_LEVEL) {char ch; cin>>ch;} }

#define VERBOSE(X) {cerr<<"INFO :: "<<X<<endl;}
#define VERBOSER(Y, X) {if(Y > DEBUG_LEVEL)cerr<<"INFO ("<<Y<<") :: "<<X;}

/*---------------------------------
 
 Macros for fatal and lower-level errors.
 
 ---------------------------------*/
#define UNRECOVERABLE_ERROR(X) {cerr<<"UNRECOVERABLE ERROR :: "<<X;assert(0);exit(0);}

#define QUERY_UNRECOVERABLE_ERROR(Q,X) {if(Q)UNRECOVERABLE_ERROR(X);}

#define WARNING(X) {}//{cerr<<"WARNING :: "<<X<<endl;}


/**********************************************************/
/* DLS specific variables                                 */
/**********************************************************/
typedef int cw_t;

typedef int prob_t;

/***********************************************************/
/* Wrappers                                                */
/***********************************************************/
#define Lit(CLAUSE, POSITION) (clauses[CLAUSE][POSITION])
#define Var(CLAUSE, POSITION) (abs(Lit(CLAUSE,POSITION)))

/**********************************************************/
/* Constants                                              */
/**********************************************************/
#define MAXVARS     5000000           // maximum possible number of variables
#define MAXCLAUSES  50000000          // maximum possible number of clauses
#define MAXLENGTH   500               // max num of literals which can be in a clause

#define BIG         100000000         // quasi-infinity for integer

#define DEFAULT_NUMRUNS      1        // default number of runs
#define DEFAULT_TIMEOUT      10       // default maximal number of seconds per try
#define DEFAULT_TARGETCOST   0        // default cost to reach in order to have a solution
#define DEFAULT_PRINTSOL     false    // default flag determining whether to output solution


#define DEFAULT_THREADS 1;
#define DEFAULT_TABU 50;

/**********************************************************/
/*   Forward declarations                                 */
/**********************************************************/
void parseParameters(int argc,char *argv[]);
void parseParametersComp(int argc,char *argv[]);
void scanInt(int argc, char *argv[], int i, int *varptr);
void scanFloat(int argc, char *argv[], int i, float *varptr);
void printHeader();
void printStatsHeader();
void printStatsEndTry();
void printFinalStats() ;
void printFinalStatsComp()  ;

void printSolution();

void releaseMemory();


/**********************************************************/
/* General parameters                                     */
/**********************************************************/
extern
int seed;                    // seed used for randomization
extern
int nbRuns;                  // number of runs
extern
int timeout;                 // maximal number of seconds per try
extern
int target;                  // cost to reach in order to have a solution
extern
int printSol;                // flag determining whether to output solution

extern
double expertime;


/**********************************************************/
/* Basic variables and data structures                    */
/**********************************************************/
extern
int nbVars;                  // number of variables in input formula
extern
int nbCls;                   // number of clauses in input formula
extern
int nbLits;                  // number of literals in input formula
extern
int maxLits;

extern
int **clauses;               // clauses -> literals : clauses[i][0]   - the size
extern
int **occurs;                // literals -> clauses : occurs[i][0]    - the size

extern
int **neighbours;

static const
int NONE = -1;

extern
bool *solution;	             // where the last solution gets saved
extern
int  totalCost;               // sum of costs associated w/ all false clauses
extern
int  lowCost;                 // best totalCost seen so far
extern
long lowFlips;                // number of flips it took to reach lowCost

extern
int    finalCost;
extern
long   finalFlips;
extern
double finalRuntime;

extern
int  nbThreads;

extern
int  tabuSize;

/**********************************************************/
/* ANOV parameters                                        */
/**********************************************************/
extern
prob_t GLOBAL__noise;
extern
prob_t GLOBAL__smoothProb;
extern
prob_t GLOBAL__walkProb;    // default random walk probability


/**********************************************************/
/*  Global Statistics                                     */
/**********************************************************/
extern
long totalFlips;              // total number of flips in all runs so far
extern
long totalNullFlips;          // total num of nullFlips in all runs so far
extern
long totalSuccessFlips;       // total num of flips in succesful runs so far
extern
long totalSuccessNullFlips;   // total num of null flips in succesful runs so far
extern
int  successRuns;             // total number of successful runs so far

#endif

