TARGET=gnovelty+

#FLEX_LIB=-L/usr/ -lfl

#RANLIB	= ranlib

CXX = g++  # -Wall -g -pg -ggdb -pthread 
#CXX = g++ -DUSE_setOfStates -O3 -pthread # -Wall -g -pg -ggdb -pthread 
#CXX = g++ -DUSE_setOfStates -O3 -march=native -pthread # -Wall -g -pg -ggdb -pthread 

#CXX = g++ -O3 -static -fno-strict-aliasing -pthread
#CXX = g++ -DUSE_setOfStates -static-libgcc -DNDEBUG -O3 -fno-strict-aliasing -fomit-frame-pointer -funroll-all-loops -fexpensive-optimizations -malign-double -Wall -march=nocona -ffast-math -pthread #-s -DOLD_FLEX -DNDEBUG -O3 -fno-strict-aliasing -fomit-frame-pointer -funroll-all-loops -fexpensive-optimizations   -mno-align-stringops  -minline-all-stringops -pipe  -mfpmath=sse -m32 -msse3 -falign-functions=64 -fforce-addr -ffast-math -mpush-args  -maccumulate-outgoing-args -momit-leaf-frame-pointer -pthread

#CXX = g++ -DUSE_setOfStates -O3 -Wall -ggdb


MODULES= State PartialState global rtime satcnf gnovelty+ main

OBJECTS=$(MODULES:=.o) $(GENERATED:=.o) 


LDFLAGS=$(FLEX_LIB) -lz \
	-lm -lpthread  
#	-lrt -lm -lpthread  

#CFLAGS= -I.  -mtune=native  -fsched2-use-superblocks -fprofile-arcs -fprofile-generate=/home/cgretton/Work2012/Software/SAT/gnovelty+-T/Profiles/ -DUSE_setOfStates_withTabu -DUSE_setOfStates -O3 -pthread 
#CFLAGS= -I.  -mtune=native  -fsched2-use-superblocks -fbranch-probabilities -fprofile-use=/home/cgretton/Work2012/Software/SAT/gnovelty+-T/Profiles/ -DUSE_setOfStates_withTabu -DUSE_setOfStates -O3 -pthread 
CFLAGS= -I. -DUSE_setOfStates_withTabu -DUSE_setOfStates -pthread -ggdb

all: $(GENERATED:=.cpp) $(TARGET)

$(TARGET): $(OBJECTS) 
	$(CXX) $(CFLAGS) -o $@ $(OBJECTS) $(CFLAGS) $(LDFLAGS)

%.o: %.cpp
	$(CXX) $(CFLAGS) $< -c -o $@

clean:
	rm -f $(OBJECTS) $(TARGET)
