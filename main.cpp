/************************************************************/
/* gnovelty+.hpp, version 1.2                               */
/*                                                          */
/*      A greedy gradient based Novelty+                    */
/*                                                          */
/*      1. Duc Nghia Pham (duc-nghia.pham@nicta.com.au)	    */ 
/*            National ICT Australia Ltd.                   */
/*            IIIS, Griffith University, Australia          */
/*                                                          */
/*      2. Charles Gretton (charles.gretton@computer.org)   */ 
/*            National ICT Australia Ltd. 2006/09; 2011/12  */
/*            University of Birmingham (2009-2011)          */
/*            IIIS, Griffith University, Australia 2006/12  */
/*                                                          */
/*                                                          */
/************************************************************/
/*    Consult legal.txt for legal information    		  */
/**********************************************************/
/* Partial of the code is based on UBCSAT version 1.0     */
/* written by:                                            */
/* Dave A.D. Tompkins & Holger H. Hoos                    */
/*   Univeristy of British Columbia                       */
/* February 2004                                          */
/**********************************************************/

/************************************/
/* Standard includes                */
/************************************/
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <cerrno>
#include <cstring>

#include "global.hpp"
#include "gnovelty+.hpp"
#include "satcnf.hpp"
#include "rtime.hpp"

using namespace std;

int exitCode = 0;

extern bool no_solution_yet;

/**********************************************************/
/* Main Program                                           */
/**********************************************************/
int main(int argc,char *argv[]) {
	pthread_mutex_init( &solution_mutex, NULL );
	pthread_mutex_init( &running_mutex, NULL );
	
	VERBOSER(1, "Printing headder.\n");
	printHeader();
	
	seed = genRandomSeed();
	parseParameters(argc, argv);
//	parseParametersComp(argc, argv);
	srandom(seed);
        srand48(seed);
	
	cout << "c #threads  = " << nbThreads << endl;
	
	VERBOSER(1, "Done generation of random seeds.\n");
	
	if ( !parseCNF(argv[1]) ) {
		cerr << "ERROR: Fail to parse the CNF file '" << argv[1] << "' !!!" << endl;
		exit(-1);
	}
	findNeighbours();
	
    totalFlips = totalNullFlips = totalSuccessFlips = totalSuccessNullFlips = 0;
    successRuns = 0;
	
	VERBOSER(1, "Done initialisation of instances.\n");
	
	printStatsHeader();

	initTimeCounter();
	
	GNoveltyPlus* gthread;

	int local__running_count = 0;
	int hash_array_size = 100000;
	
	for ( int run = 1; run <= nbRuns; ++run ) {
		no_solution_yet = true;
		lowCost         = BIG;
		lowFlips        = 0;

		for ( int i = 0; i < nbThreads && no_solution_yet; ++i ) {
			gthread = new GNoveltyPlus(nbVars, nbCls, hash_array_size++, tabuSize);

			int result = pthread_create(&gthread->thread,
                                                    &gthread->attributes,
                                                    runGNoveltyPlus,
                                                    reinterpret_cast<void*>(gthread));
			if ( result ) {
                #ifdef _POSIX_THREAD_THREADS_MAX
					WARNING("Failed to spawn a thread. "
							<<"Perhaps the upper limit on the number we can spawn is :: "
							<<_POSIX_THREAD_THREADS_MAX<<".");
				#endif
                
				switch ( result ) {
					case EAGAIN:
						WARNING("Your system lacked the necessary resources"
								<<" to create another thread, OR your system-imposed"
								<<" limit  on  the  total  number  of threads in"
								<<" a process {PTHREAD_THREADS_MAX} would be exceeded. Nice grammar!!"
								<<" Anyway, I will try again after waiting for threads"
								<<" I am perhaps responsible for...");
						pthread_exit(0);
						break;
					case EINVAL:
						UNRECOVERABLE_ERROR("My bad. I specified thread attribute values that are invalid."
											<<" Nothing to be done.. so I am killing myself "
											<<"-- i.e., \"pthread_exit(0)\".");

						pthread_exit(0);
						break;
					case EPERM:
						UNRECOVERABLE_ERROR("The caller does not have appropriate permission to"
											<<" set the required scheduling parameters or"
											<<" scheduling policy. Whatever that means ;)"
											<<" Nothing to be done, I am killing myself -- "
											<<"-- i.e., \"pthread_exit(0)\".");
						pthread_exit(0);
						break;
					case ENOMEM:
						UNRECOVERABLE_ERROR("Oh great! pthread_create() just gave me a ENOMEM. "
											<<"This undocumented behaviour occurs under Redhat Linux 2.4 "
											<<"when too many threads have been created in the non-detached "
											<<"mode, and the limited available memory in some system stack "
											<<"is consumed. At that point no new threads can be created "
											<<"in non-detached mode until those threads are "
											<<"detached/killed, or the parent process(es) killed and restarted. "
											<<"My behaviour here is to crash, better luck next time chief!");
						pthread_exit(0);
						break;
					default:
						WARNING("No idea why we couldn't spawn a thread. The error code we were given is :: "
								<<result<<" Trying again...");
						pthread_exit(0);
						break;
				}

				UNRECOVERABLE_ERROR("Failed to create thread number :: " << hash_array_size << endl);
			} else {
				increment__running_count();
			}
		}

		bool threads_still_running = true;
		while ( threads_still_running ) { 
			while ( pthread_mutex_trylock(&running_mutex) == EBUSY ) sleep(.001);
			if ( running_count == 0 ) 
				threads_still_running = false;
			if ( 0 != pthread_mutex_unlock(&running_mutex) ) 
				cerr << "Unable to unlock mutex." << endl;
		}
		
		printStatsEndTry();
	}
	
	expertime = elapsedTime();
//	printFinalStats();
	printFinalStatsComp();

	cerr << "c Releasing memory..." << endl;
	releaseMemory();

	cerr << "c ExitCode: " << exitCode << endl;
	return exitCode;
}

void releaseMemory() {
	if ( clauses ) {
		for ( int i = 0; i < nbCls; ++i )
			if ( clauses[i] ) delete[] clauses[i];
		delete[] clauses;
	}
	if ( occurs ) {
		for ( int i = 0; i < maxLits; ++i )
			if ( occurs[i] ) delete[] occurs[i];
		delete[] occurs;
	}
	if ( neighbours ) {
		for ( int i = 1; i <= nbVars; ++i )
			if ( neighbours[i] ) delete[] neighbours[i];
		delete[] neighbours;
	}
	if ( solution ) delete[] solution;
}

void parseParameters(int argc,char *argv[]) {
	int i;

	nbRuns     = DEFAULT_NUMRUNS;
	timeout    = DEFAULT_TIMEOUT;
	target     = DEFAULT_TARGETCOST;
	printSol   = DEFAULT_PRINTSOL;
	
	nbThreads  = DEFAULT_THREADS;
	tabuSize  = DEFAULT_TABU;
	
	for (i = 2; i < argc; i++) {
		if (strcmp(argv[i],"-seed") == 0) scanInt(argc,argv,++i,&seed);
		else if (strcmp(argv[i],"-threads") == 0) scanInt(argc,argv,++i,&nbThreads);
		else if (strcmp(argv[i],"-tabu") == 0) scanInt(argc,argv,++i,&tabuSize);
		else if (strcmp(argv[i],"-timeout") == 0) scanInt(argc,argv,++i,&timeout);
		else if (strcmp(argv[i],"-runs") == 0) scanInt(argc,argv,++i,&nbRuns);
		else if (strcmp(argv[i],"-target") == 0) scanInt(argc,argv,++i,&target);
		else if (strcmp(argv[i],"-noise") == 0) scanInt(argc,argv,++i,&GLOBAL__noise);
		else if (strcmp(argv[i],"-walkprob") == 0) scanInt(argc,argv,++i,&GLOBAL__walkProb);
		else if (strcmp(argv[i],"-smoothprob") == 0) scanInt(argc,argv,++i,&GLOBAL__smoothProb);
		else if (strcmp(argv[i],"-printsol") == 0) printSol = true;
		else {
			if (strcmp(argv[i],"-help")!=0 && strcmp(argv[i],"-h")!=0 )
				fprintf(stderr, "\nBad argument %s\n\n", argv[i]);
			fprintf(stderr, "General parameters:\n");
			fprintf(stderr, "  -seed N (integer, default: quasi-random based on system time)\n");
			fprintf(stderr, "  -timeout N (integer, default: %d)\n", timeout);
			fprintf(stderr, "  -runs N (integer, default: %d)\n", nbRuns);
			fprintf(stderr, "  -noise N (percent, default: %d)\n", GLOBAL__noise);
			fprintf(stderr, "  -walkprob N (percent, default: %d)\n", GLOBAL__walkProb);
			fprintf(stderr, "  -smoothprob N (percent, default: %d)\n", GLOBAL__smoothProb);
			fprintf(stderr, "  -target N = find assignments of cost <= N (MAXSAT) (integer, default: %d)\n", target);
			fprintf(stderr, "  -printsol\n");
			fprintf(stderr, "\nUsage:\n");
			fprintf(stderr, "  sls -parameters < instance.cnf\n\n");
			exit(-1);
		}
	}
}

void parseParametersComp(int argc,char *argv[]) {
	nbRuns     = DEFAULT_NUMRUNS;
	timeout    = DEFAULT_TIMEOUT;
	target     = DEFAULT_TARGETCOST;
	printSol   = DEFAULT_PRINTSOL;

	nbThreads  = DEFAULT_THREADS;

	if (argc > 2) scanInt(argc, argv, 2, &seed);
	if (argc > 3) scanInt(argc, argv, 3, &nbThreads);
}

void scanInt(int argc, char *argv[], int i, int *varptr) {
	if (i>=argc || sscanf(argv[i],"%i",varptr)!=1){
		fprintf(stderr, "Bad argument %s\n", i<argc ? argv[i] : argv[argc-1]);
		exit(-1);
	}
}


void scanFloat(int argc, char *argv[], int i, float *varptr) {
	if (i>=argc || sscanf(argv[i],"%f",varptr)!=1){
		fprintf(stderr, "Bad argument %s\n", i<argc ? argv[i] : argv[argc-1]);
		exit(-1);
	}
}

void printHeader() {
	cout << "c +------------------------------+" << endl;
	cout << "c | gNovelty+, version 1.2       |" << endl;
	cout << "c +------------------------------+" << endl;
	cout << "c gnovelty+ -h for help" << endl;
	cout << "c " << endl;
}

void printStatsHeader() {
	cout << "c Problem instance: nbVars = " << nbVars << ", nbCls = " << nbCls << endl;
	cout << "c General parameters: seed = " << seed << ", timeout = " << timeout << ", target = " << target;
	cout << ", print solution: " << ( printSol ? "yes" : "no" ) << endl;
	cout << "c gNovelty+ parameters: noise = " << GLOBAL__noise 
		 << ", walkprob = " << GLOBAL__walkProb 
		 << ", smoothprob = " << GLOBAL__smoothProb << endl;
	cout << "c runs = " << nbRuns << endl;
	cout << "c " << endl;
	
	cout << "c   best    flips     final     total     total" << endl;
	cout << "c   cost    until    #unsat     flips   seconds" << endl;
	cout << "c   this     best      this      this      this" << endl;
	cout << "c    try     soln       try      try        try" << endl;
	cout << flush;
}

void printStatsEndTry() {
	printf("c %6d %8ld %9d %9ld %9.2lf\n", lowCost, lowFlips, finalCost, finalFlips, finalRuntime);
	fflush(stdout);
}

void printFinalStats(void) {
	cout << endl;
	cout << "c total elapsed seconds = " << expertime << endl;
	cout << "c average flips per second = " << (long)( (totalFlips - totalNullFlips) / expertime ) << endl;
	cout << "c number solutions found = " << successRuns << endl;
	cout << "c final success rate = " << ( (double)successRuns * 100.0) / nbRuns << endl;
	cout << "c average length successful runs including nulls = " << (successRuns ? (totalSuccessFlips/successRuns) : 0) << endl;

	if ( successRuns > 0 )
		printf("c mean flips for successful runs = %.6f\n",
			   successRuns ? ((double) (totalSuccessFlips - totalSuccessNullFlips)/successRuns) : 0);

	printFinalStatsComp();
}

void printFinalStatsComp() {
	cout << endl;
	if ( successRuns ) {
		cout << "s SATISFIABLE" << endl;
		if ( printSol ) printSolution();
		cout << "c Done in " << expertime << " seconds" << endl;
		exitCode = 10;
	} else {
		cout << "s UNKNOWN" << endl;
		exitCode = 0;
	}
}

void printSolution() {
	cout << "v ";
	for ( int i = 1; i <= nbVars; ++i ) 
		cout << (solution[i] == 1 ? i : -i) << " ";
	cout << "0" << endl << flush;
}


