/************************************************************/
/* gnovelty+.hpp, version 1.2                               */
/*                                                          */
/*      A greedy gradient based Novelty+                    */
/*                                                          */
/*      1. Duc Nghia Pham (duc-nghia.pham@nicta.com.au)	    */ 
/*            National ICT Australia Ltd.                   */
/*            IIIS, Griffith University, Australia          */
/*                                                          */
/*      2. Charles Gretton (charles.gretton@computer.org)   */ 
/*            National ICT Australia Ltd. 2006/09; 2011/12  */
/*            University of Birmingham (2009-2011)          */
/*            IIIS, Griffith University, Australia 2006/12  */
/*                                                          */
/*                                                          */
/************************************************************/

#include "PartialState.hpp"


PartialState::PartialState(const vector<uint>& toTrack)
    :State(toTrack.size())
{
    for(uint i = 0; i < toTrack.size(); i++){
        mapPropToStatePosition[toTrack[i]] = i;
        CLONE__mapPropToStatePosition.push_back(pair<uint, uint>(toTrack[i], i));
    }
}

PartialState::PartialState(const PartialState& partialState)
    :State(partialState)
{
    mapPropToStatePosition = partialState.mapPropToStatePosition;
    CLONE__mapPropToStatePosition = partialState.CLONE__mapPropToStatePosition;
}

PartialState& PartialState::operator=(const PartialState& partialState)
{
    this->mapPropToStatePosition = partialState.mapPropToStatePosition;
    this->CLONE__mapPropToStatePosition = partialState.CLONE__mapPropToStatePosition;
    State::operator=(partialState);
    return *this;
}



bool PartialState::operator==(const PartialState& partialState) const
{
    return (CLONE__mapPropToStatePosition.size() == partialState.CLONE__mapPropToStatePosition.size()) &&
        (CLONE__mapPropToStatePosition == CLONE__mapPropToStatePosition) &&
        (State::operator==(partialState));
}

bool PartialState::operator<(const PartialState& partialState) const
{
//     CLONE__mapPropToStatePosition < CLONE__mapPropToStatePosition;
    
    if(CLONE__mapPropToStatePosition.size() < partialState.CLONE__mapPropToStatePosition.size()){
        return true;
    } else if (CLONE__mapPropToStatePosition.size() == partialState.CLONE__mapPropToStatePosition.size()) {
        if(CLONE__mapPropToStatePosition < partialState.CLONE__mapPropToStatePosition){
            return true;
        } else if (CLONE__mapPropToStatePosition == partialState.CLONE__mapPropToStatePosition){
            return State::operator<(partialState);
        }
    }

    return false;
}

