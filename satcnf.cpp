/************************************************************/
/* gnovelty+.hpp, version 1.2                               */
/*                                                          */
/*      A greedy gradient based Novelty+                    */
/*                                                          */
/*      1. Duc Nghia Pham (duc-nghia.pham@nicta.com.au)	    */ 
/*            National ICT Australia Ltd.                   */
/*            IIIS, Griffith University, Australia          */
/*                                                          */
/*      2. Charles Gretton (charles.gretton@computer.org)   */ 
/*            National ICT Australia Ltd. 2006/09; 2011/12  */
/*            University of Birmingham (2009-2011)          */
/*            IIIS, Griffith University, Australia 2006/12  */
/*                                                          */
/*                                                          */
/************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
//#include <string>
#include <cstdlib>
#include <cassert>
#include <algorithm>

#include "global.hpp"
#include "satcnf.hpp"


#define error(MSG) { \
	std::cerr << "c ERROR: file " << inFile << " " << MSG << "!!!" << std::endl << std::flush;  \
	fin.close();     \
	return false;    \
}

#define error1(MSG) { \
	std::cerr << "c ERROR: " << MSG << "!!!" << std::endl << std::flush;  \
	fin.close();     \
	return false;    \
}

static
bool compareVAR(const int& a, const int& b) {
    if (std::abs(a) == std::abs(b)) {
		return (a < b);
	}
	return (std::abs(a) < std::abs(b));
}


bool parseCNF(char *inFile) {
	std::ifstream fin;

	fin.open(inFile, std::ios::in);
	
	if ( !fin ) error("cannot be opened");

	// skip all the commence
	std::string inLine = "c A NULL string";
	while ( !fin.eof() ) {
		std::getline(fin, inLine);
		if ( inLine[0] == 'p' ) break;
	};

	// check the cnf type
	if ( fin.eof() || inLine.compare(0, 5, "p cnf") ) error("is not a CNF file");
	
	// read in nbVars and nbCls
	if ( !( std::istringstream(inLine.substr(5, inLine.length()-5)) >> nbVars >> nbCls ) )
		error("is not a CNF file");
	
	std::cout << "c p cnf " << nbVars << " " << nbCls << std::endl << std::flush;
	
	maxLits = (nbVars + 1) << 1;
	clauses = new int*[nbCls];
	if ( !clauses ) error1("cannot parse CNF clauses due to running out of memory");

	solution = new bool[nbVars + 1];
	if ( !solution ) error1("running out of memory");
	
	// ****************************************************************
	// read in all the clauses
	int *szOccurs = new int[maxLits];
	for ( register int i = 0; i < maxLits; ++i ) szOccurs[i] = 0;
	
	int *cls = new int[MAXLENGTH + 1];
	int lit  = 0;
	
	int _nbCls = nbCls;
	nbCls = 0;
	for ( register int i = 0; i < _nbCls; ++i ) {
		int cntLits = 0;

		assert(fin >> lit);
		while ( lit != 0 ) {
			assert(abs(lit) <= nbVars);
			
			if ( ++cntLits > MAXLENGTH ) {
				delete [] cls;
				std::ostringstream ss;
				ss << "CNF clause " << i 
					<< " exceeds the maximum allowed length " << MAXLENGTH;
				error1(ss.str());
			}
			cls[cntLits] = lit;

			assert(fin >> lit);
		}
		
		if ( cntLits ) {
			std::sort(cls+1, cls+cntLits+1, compareVAR);
			
			clauses[nbCls]    = new int[cntLits+1];
			clauses[nbCls][0] = cntLits;
			for (int j = 1, *ptr = cls+1; j <= cntLits; ++j, ++ptr) {
				clauses[nbCls][j] = *ptr;
				++szOccurs[nbVars + *ptr];
			}
			++nbCls;
		}
	}
	delete [] cls;	cls = NULL;
	fin.close();

	// ****************************************************************
	// establish the occurrences array
	occurs = new int*[maxLits];
	if ( !occurs ) error1("cannot parse CNF clauses due to running out of memory");

	for ( int i = 0; i < maxLits; ++i ) {
		occurs[i] = new int[szOccurs[i] + 2];
		if ( !occurs[i] ) error1("cannot parse CNF clauses due to running out of memory");
		occurs[i][0] = 0;
	}
	delete [] szOccurs;	szOccurs = NULL;

	for ( int i = 0; i < nbCls; ++i ) {
		for ( int j = 1; j <= clauses[i][0]; ++j ) {
			lit = nbVars + clauses[i][j];
			occurs[lit][ ++occurs[lit][0] ] = i;
		}
	}

	for ( int i = 0; i < maxLits; ++i )
		occurs[i][ occurs[i][0] + 1 ] = NONE;
	
	return true;
}

