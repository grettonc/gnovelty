/************************************************************/
/* gnovelty+.hpp, version 1.2                               */
/*                                                          */
/*      A greedy gradient based Novelty+                    */
/*                                                          */
/*      1. Duc Nghia Pham (duc-nghia.pham@nicta.com.au)	    */ 
/*            National ICT Australia Ltd.                   */
/*            IIIS, Griffith University, Australia          */
/*                                                          */
/*      2. Charles Gretton (charles.gretton@computer.org)   */ 
/*            National ICT Australia Ltd. 2006/09; 2011/12  */
/*            University of Birmingham (2009-2011)          */
/*            IIIS, Griffith University, Australia 2006/12  */
/*                                                          */
/*                                                          */
/************************************************************/

#ifndef STATE_HH
#define STATE_HH

typedef long long unsigned int lui;
typedef unsigned int uint;

#define ELEM_TYPE lui
//#define ELEM_TYPE long long unsigned

/*Number of bits in each element of the bitvector*/
#define SIZE_ELEM (sizeof(ELEM_TYPE) * 8)


#define CEIL(X) (( X % SIZE_ELEM )?( ( X / SIZE_ELEM ) + 1):(X / SIZE_ELEM))
#define FLOOR(X) (X / SIZE_ELEM)
#define REMAINDER(X) (X % SIZE_ELEM)

/*---------------------------------

  Old fashioned debugging

  ---------------------------------*/
#define DEBUG_LEVEL 3
#define DEBUG_GET_CHAR(Y) {if(Y > DEBUG_LEVEL) {char ch; cin>>ch;} }

#define VERBOSE(X) {cerr<<"INFO :: "<<X<<endl;}
#define VERBOSER(Y, X) {if(Y > DEBUG_LEVEL)cerr<<"INFO ("<<Y<<") :: "<<X;}

/*---------------------------------

  Macros for fatal and lower-level errors.

  ---------------------------------*/
#define UNRECOVERABLE_ERROR(X) {cerr<<"UNRECOVERABLE ERROR :: "<<X;assert(0);exit(0);}

#define QUERY_UNRECOVERABLE_ERROR(Q,X) {if(Q)UNRECOVERABLE_ERROR(X);}

#define WARNING(X) {}//{cerr<<"WARNING :: "<<X<<endl;}

/*---------------------------------

  Usual suspects C++::1998

  ---------------------------------*/

/*IO, string, files*/
#include<string>
#include<iostream>
#include<iomanip>
#include<sstream>

/*Storage*/
#include<vector>
#include<map>
#include<set>

/*Sorting, and set operations*/
#include<algorithm>

/*---------------------------------

  Usual suspects C++::TR1

  ---------------------------------*/

#include <tr1/unordered_map>
#include <tr1/unordered_set>

/*Inlcude for boost hashing et al.*/
#include <boost/functional/hash.hpp>

/*Model is stored in "atom" and the range is "1 to numAtoms+1".*/

using namespace std;


class Basic_State {
public:	
    /*Initially nothing is true (all bits in \member{data} are
     * false). \argument{size} is the number of propositions that
     * this state is required to keep the truth value of.*/
    Basic_State(uint size = 0);

    ~Basic_State();
    
    /*Copy construction clones \member{numPropositions},
     * \member{data}, \member{time} and
     * \member{possibleActions}. NOTE: We DO NOT copy
     * \member{propositionsChanged}.*/
    Basic_State(const Basic_State&);

    /*This clones the same elements as the copy constructor.*/
    Basic_State& operator=(const Basic_State&);
	
    /*Comparison is based on the size of \member{data} first, and
     * then on the actual elements.*/
    bool operator!=(const Basic_State& state) const{return !this->operator==(state);}
    bool operator==(const Basic_State& state) const;
    bool operator<(const Basic_State& state) const;
	
    static const ELEM_TYPE big = -1;

    
//     /*Change individual (at index \argument{uing}) bits of the
//      * state representation (\member{data}).*/
//     void flipOn(uint);
//     void flipOff(uint);
//     void flip(uint);

//     /*Is proposition \argument{uint} true?*/
//     bool isTrue(uint) const;
    inline bool isFalse(uint in) const {return !isTrue(in);};


    /*Change individual (at index \argument{uing}) bits of the
     * state representation (\member{data}).*/
    inline void flipOn(uint index)
    {
        register uint remainder = REMAINDER(index);
        register uint bitVecNum = FLOOR(index);

        assert(bitVecNum < CEIL(numPropositions));
        assert(bitVecNum >=0);
        
//         VERBOSER(2, "Flipping ::"<<index<<endl
//                  <<remainder<<endl
//                  <<bitVecNum<<endl);
        
        register ELEM_TYPE mask = 1;
        mask = mask<<remainder;
        data[bitVecNum] |= mask;
    
        /*Now it's true, some actions may be executable.*/
    }

    inline void flipOff(uint index)
    {
        register uint remainder = REMAINDER(index);
        register uint bitVecNum = FLOOR(index);

        assert(bitVecNum < CEIL(numPropositions));
        assert(bitVecNum >=0);
        
        register ELEM_TYPE mask = 1;
        mask = mask<<remainder;
        mask = mask^big;
        data[bitVecNum] &= mask;
    }

    inline void flip(uint index)
    {
        register uint remainder = REMAINDER(index);
        register uint bitVecNum = FLOOR(index);

        assert(bitVecNum < CEIL(numPropositions));
        assert(bitVecNum >=0);
        
        register ELEM_TYPE mask = 1;
        mask = mask<<remainder;
    
        if(data[bitVecNum] & mask){
            mask = mask^big;
            data[bitVecNum] &= mask;
        } else {
            data[bitVecNum] |= mask;

            /*Now it's true, some actions may be executable.*/
        }
    }

    inline bool isTrue(uint index) const
    {
        register uint remainder = REMAINDER(index);
        register uint bitVecNum = FLOOR(index);

        assert(bitVecNum < CEIL(numPropositions));
        assert(bitVecNum >=0);
        register ELEM_TYPE mask = 1;
        mask = mask<<remainder;
    
        return (data[bitVecNum] & mask)?true:false;
    }





    
    void randomize();
    
    size_t hash_value() const;

    size_t new_hash_if_flip(uint index);

    /*see \member{numPropositions}.*/
    uint getNumPropositions() const{return numPropositions;};
private:
//     /* Bitmask for use by accessor functions such as flipOn, flipOff,
//      * isTrue, etc.*/
//     register uint mask;

//     /*(see \member{flipOff}, \member{flipOn}, \member{}flipOn, etc.)*/
//     register uint bitVecNum;
    
//     /*(see \member{flipOff}, \member{flipOn}, \member{}flipOn, etc.)*/
//     register uint remainder;


    
    /*Number of propositions in the state description.*/
    uint numPropositions;
	
    /*64 bits per element in this vector, each bit represents the
     * truth value of a proposition.*/
    //vector<lui> data;
    ELEM_TYPE* data;
};
    
/*Function for STL and boost to access \member{hash_value} of
 * \argument{GroundAction}.*/
std::size_t hash_value(const Basic_State& );


template<typename Storage, size_t size_elem = 0>
class Block_State {
public:	
    inline size_t local_ceil(size_t index) const {return (( index % size_elem )?( ( index / size_elem ) + 1):(index / size_elem));}
    inline size_t local_floor(size_t index) const {return index / size_elem;}
    inline size_t remainder(size_t index) const {return index % size_elem;}

    Block_State(size_t size = 0)
        :numPropositions(size),
         data(0),
         cached_hashes(0)
    {
        // cerr<<"New block with props count:: "<<numPropositions<<" block-size "
        //     <<size_elem<<" with entries count "<<local_ceil(numPropositions)<<std::endl;

        size_t dimension = local_ceil(numPropositions);
        data = new Storage[dimension];
        for(uint i = 0; i < local_ceil(numPropositions); i++){
            //cerr<<"New block, item :: "<<i<<" block-size "<<size_elem<<" with entries count "<<local_ceil(numPropositions)<<std::endl;
            data[i] = Storage(size_elem);
        }
        cached_hashes = new uint[dimension];
        for(uint i = 0; i < local_ceil(numPropositions); i++){
            cached_hashes[i] = 0;
        }
    }
    
    ~Block_State()
    {
        //cerr<<"Delete block :: "<<size_elem<<std::endl;
        if(data)delete [] data;
        if(cached_hashes)delete [] cached_hashes;
    }

    Block_State(const Block_State&in)
        :numPropositions(in.numPropositions),
         data(0),
         cached_hashes(0)
    {
        //cerr<<"New copy block :: "<<size_elem<<std::endl;
        

        
        size_t dimension = local_ceil(numPropositions);
        data = new Storage[dimension];
        for(uint i = 0; i < local_ceil(numPropositions); i++){
            data[i] = Storage(size_elem);
        }

        cached_hashes = new uint[dimension];
        for(uint i = 0; i < local_ceil(numPropositions); i++){
            cached_hashes[i] = 0;
        }
    }

    Block_State& operator=(const Block_State&in)
    {
        //cerr<<"Block assignment :: "<<size_elem<<std::endl;
        
        numPropositions = in.numPropositions;
        if(data) delete [] data;
        if(cached_hashes) delete [] cached_hashes;

        size_t dimension = local_ceil(numPropositions);
        data = new Storage[dimension];
        for(uint i = 0; i < local_ceil(numPropositions); i++){
            data[i] = Storage(size_elem);
        }
        cached_hashes = new uint[dimension];
        for(uint i = 0; i < local_ceil(numPropositions); i++){
            cached_hashes[i] = 0;
        }
    }
    bool operator!=(const Block_State& state) const{return !operator==(state);}
    bool operator==(const Block_State& state) const{
        if(numPropositions != state.numPropositions) return false;
    
        for(uint i = 0; i < local_ceil(numPropositions); i++){
            if(data[i] != state.data[i]) {
                return false;
            }
        }
        
        return true;//state.data == this->data;
    }
    // bool operator>(const Block_State& state) const{
    //     return !operator<(state) && !operator==(state);
    // }
    bool operator<(const Block_State& state) const{
        
        if(numPropositions > state.numPropositions){
            return false;
        } else if (numPropositions == state.numPropositions) {
            for(uint i = 0; i < local_ceil(numPropositions); i++){
                if (data[i] < state.data[i]) {
                    return true;
                } else if (data[i] != state.data[i]){
                    return false;
                }
            }
        } else {
            return true;
        }
    
        assert(*this == state);
    
        return false;
    }

    

    inline void flipOn(uint index)
    {
        register uint remain = remainder(index);
        register uint bitVecNum = local_floor(index);

        //std::cerr<<" Elems of size :: "<<size_elem<<" with lotal props "<<numPropositions<<std::endl;
        //std::cerr<<"Testing truth value of :: "<<index<<" which is in entry :: "<<bitVecNum<<" item # "<<remain<<std::endl;
        assert(bitVecNum < local_ceil(numPropositions));
        assert(bitVecNum >=0);

        if(data[bitVecNum].isTrue(remain))return;
        data[bitVecNum].flipOn(remain);
        cached_hashes[bitVecNum] = data[bitVecNum].hash_value();
    }

    inline void flipOff(uint index)
    {
        register uint remain = remainder(index);
        register uint bitVecNum = local_floor(index);

        assert(bitVecNum < local_ceil(numPropositions));
        assert(bitVecNum >=0);
        
        //std::cerr<<"Testing truth value of :: "<<index<<" which is in entry :: "<<bitVecNum<<" item # "<<remain<<std::endl;
        if(data[bitVecNum].isFalse(remain))return;
        
        data[bitVecNum].flipOff(remain);
        cached_hashes[bitVecNum] = data[bitVecNum].hash_value();
    }

    inline void flip(uint index)
    {
        register uint remain = remainder(index);
        register uint bitVecNum = local_floor(index);

        assert(bitVecNum < local_ceil(numPropositions));
        assert(bitVecNum >=0);
        if(data[bitVecNum].isTrue(remain)){
            data[bitVecNum].flipOff(remain);
        } else {
            data[bitVecNum].flipOn(remain);
        }
        cached_hashes[bitVecNum] = data[bitVecNum].hash_value();
    }

    inline bool isFalse(uint in) const {return !isTrue(in);};
    
    inline bool isTrue(uint index) const
    {
        register uint remain = remainder(index);
        register uint bitVecNum = local_floor(index);

        assert(bitVecNum < local_ceil(numPropositions));
        assert(bitVecNum >=0);
        return data[bitVecNum].isTrue(remain);
    }
    
    inline size_t new_hash_if_flip(uint index)
    {
        //std::cerr<<"Testing the hash value of a flip."<<std::endl;
        register uint remain = remainder(index);
        register uint bitVecNum = local_floor(index);
        assert(bitVecNum < local_ceil(numPropositions));
        assert(bitVecNum >=0);
        size_t new_hash = data[bitVecNum].new_hash_if_flip(remain);
        size_t old_hash = cached_hashes[bitVecNum];
        cached_hashes[bitVecNum] = new_hash ;
        size_t result = hash_value();
        cached_hashes[bitVecNum] = old_hash;

        return result;
    }
    
    inline size_t hash_value() const{
        register size_t seed = 0;
        register uint i;
        
        for(i = 0; i < local_ceil(numPropositions); i++){
            boost::hash_combine(seed, cached_hashes[i]);
        }
    
        return seed;
    }

    /*see \member{numPropositions}.*/
    uint getNumPropositions() const{return numPropositions;};
private:
    /*Number of propositions in the state description.*/
    uint numPropositions;
    
    uint* cached_hashes;
    Storage* data;
};
    

template<typename Storage, size_t size_elem>
inline std::size_t hash_value(const Block_State<Storage, size_elem>& bs){
    return bs.hash_value();
}

//typedef Basic_State State;
//typedef Block_State<Basic_State, 64> State;
//typedef Block_State<Basic_State, 512> State;
//typedef Block_State<Block_State<Basic_State, 256>, 1024> State;


// c   best    flips     final     total     total
// c   cost    until    #unsat     flips   seconds
// c   this     best      this      this      this
// c    try     soln       try      try        try
// c 901312     1657    940561      3247    120.37
// c 910872     2153   1008875      2680    120.03
// c 877413      452   1059911         4    121.10
// c 890453     1219    919886      3326    120.10



// 138563 -- Variables...
typedef Block_State<Basic_State, 640> Crafted_Basics;
typedef Block_State<Crafted_Basics, 2560> Crafted_L1;
typedef Block_State<Crafted_L1, 10240> Crafted_L2;
typedef Block_State<Crafted_L2, 10240 * 4> Crafted_L3;
//typedef Crafted_L2 State;
//typedef Crafted_L3 State;


//#define THREE_SIZE 128
//#define THREE_SIZE 256
#define THREE_SIZE 512
//#define THREE_SIZE 1024
//#define THREE_SIZE 2048
typedef Block_State<Basic_State, THREE_SIZE> Three_Basics;
typedef Block_State<Three_Basics, THREE_SIZE * 3> Srq3_Basics;
typedef Block_State<Srq3_Basics, THREE_SIZE * 3 * 3> Cub3_Basics;
typedef Block_State<Cub3_Basics, THREE_SIZE * 3 * 3 * 3> Quart3_Basics;
typedef Block_State<Quart3_Basics, THREE_SIZE * 3 * 3 * 3 * 3> Quin3_Basics;
typedef Block_State<Quin3_Basics, THREE_SIZE * 3 * 3 * 3 * 3 * 3> Sex3_Basics;
typedef Block_State<Sex3_Basics, THREE_SIZE * 3 * 3 * 3 * 3 * 3 * 3> Sept3_Basics;
typedef Block_State<Sept3_Basics, THREE_SIZE * 3 * 3 * 3 * 3 * 3 * 3 * 3> Oct3_Basics;
typedef Block_State<Oct3_Basics, THREE_SIZE * 3 * 3 * 3 * 3 * 3 * 3 * 3 * 3> Nov3_Basics;
typedef Block_State<Nov3_Basics, THREE_SIZE * 3 * 3 * 3 * 3 * 3 * 3 * 3 * 3 * 3> Dec3_Basics;
//typedef Oct3_Basics State;
//typedef Nov3_Basics State;
//typedef Sept3_Basics State;
//typedef Sex3_Basics State;//**
//typedef Quin3_Basics State;//#####
//typedef Quart3_Basics State;
typedef Basic_State State;

//#define TWO_SIZE 128
#define TWO_SIZE 1024
//#define TWO_SIZE 2048
typedef Block_State<Basic_State, TWO_SIZE> Two_Basics;
typedef Block_State<Two_Basics, TWO_SIZE * 2> Srq2_Basics;
typedef Block_State<Srq2_Basics, TWO_SIZE * 2 * 2> Cub2_Basics;
typedef Block_State<Cub2_Basics, TWO_SIZE * 2 * 2 * 2> Quart2_Basics;
typedef Block_State<Quart2_Basics, TWO_SIZE * 2 * 2 * 2 * 2> Quin2_Basics;
typedef Block_State<Quin2_Basics, TWO_SIZE * 2 * 2 * 2 * 2 * 2> Sex2_Basics;
typedef Block_State<Sex2_Basics, TWO_SIZE * 2 * 2 * 2 * 2 * 2 * 2> Sept2_Basics;
typedef Block_State<Sept2_Basics, TWO_SIZE * 2 * 2 * 2 * 2 * 2 * 2 * 2> Oct2_Basics;
typedef Block_State<Oct2_Basics, TWO_SIZE * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2> Nov2_Basics;
typedef Block_State<Nov2_Basics, TWO_SIZE * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2 * 2> Dec2_Basics;
//typedef Srq2_Basics State;
//typedef Cub2_Basics State;
//typedef Quart2_Basics State;
// typedef Quin2_Basics State;//**
//typedef Basic_State State;
// typedef Dec2_Basics State;
// typedef Nov2_Basics State;
// typedef Oct2_Basics State;
// typedef Sept2_Basics State;
//typedef Sex2_Basics State;

typedef Block_State<Basic_State, 256> Four_Basics;
typedef Block_State<Four_Basics, 1024> Srq4_Basics;
typedef Block_State<Srq4_Basics, 4096> Cub4_Basics;
typedef Block_State<Cub4_Basics, 16384> Quart4_Basics;
//typedef Quart4_Basics State;
//typedef Cub4_Basics State;
//typedef Four_Basics State;
//typedef Srq4_Basics State;
// c  24510    12778     24515     12789     10.01
// c  24515    12818     24515     12818     10.01
// c  24311    13270     24311     13341     10.00
// c  24755    14109     24760     14114     10.00
// c  24464    12602     24464     12602     10.00


class SetOfStateHashes //: public tr1::unordered_set<size_t, boost::hash<size_t> >
{
public:
    SetOfStateHashes(uint SIZE = 1000000)
        :array_size(CEIL(SIZE)),
         entries(SIZE),
         non_zero_count(0)
    {
//         saturation = 0;
        hash_array = new ELEM_TYPE[CEIL(SIZE)];
        for(uint i = 0; i < CEIL(SIZE); i++){
            hash_array[i] = 0;
        }
    }

    /* Number of non-zero bits in the array.*/
    int size() const{
        return non_zero_count;
    }
    int maximum_size() const {
        return entries;
    }

    SetOfStateHashes(const SetOfStateHashes& setOfStateHashes)
    {
        this->entries = setOfStateHashes.entries;
        this->array_size = setOfStateHashes.array_size;
        
        this->non_zero_count = setOfStateHashes.non_zero_count;
        
        hash_array = new ELEM_TYPE[this->array_size];
        for(uint i = 0; i < this->array_size; i++){
            hash_array[i] = 0;
        } 
    }
    
    SetOfStateHashes& operator=(const SetOfStateHashes& setOfStateHashes)
    {
        this->entries = setOfStateHashes.entries;
        this->array_size = setOfStateHashes.array_size;
        this->non_zero_count = setOfStateHashes.non_zero_count;
        
       
        hash_array = new ELEM_TYPE[this->array_size];
        for(uint i = 0; i < this->array_size; i++){
            hash_array[i] = 0;
        }

        return *this;
    }
    
    ~SetOfStateHashes()
    {
        if(hash_array)
            delete [] hash_array;
    }
    
    inline uint find(uint state_hash) 
    {
        register uint index = state_hash % entries;
        return internal_find(index);
    }
protected:
    inline uint internal_find(uint index) 
    {
        
        register uint remainder = REMAINDER(index);
        register uint bitVecNum = FLOOR(index);
        register ELEM_TYPE mask = 1;
        mask = mask<<remainder;


        if(hash_array[bitVecNum] & mask){
            return 0;
        } else {
            return entries + 1;
        }
    }
public:
    inline uint find(const State& state) 
    {
        register uint index = state.hash_value() % entries;
        return internal_find(index);
    }

    inline uint end()
    {
        return entries + 1;
    }
    
    /* Ensures the bit at \argument{index} is false. */
    inline void flip(uint index){

        register uint remainder = REMAINDER(index);
        register uint bitVecNum = FLOOR(index);
        


        register ELEM_TYPE mask = 1;
        mask = mask<<remainder;
        
        /*Already false, nothing to be flipped.*/
        if(hash_array[bitVecNum] & mask){
            non_zero_count--;
            mask = mask^big;
            hash_array[bitVecNum] &= mask;
        }
    }

    inline void insert(uint index){
        
        register uint remainder = REMAINDER(index);
        register uint bitVecNum = FLOOR(index);
        
        register ELEM_TYPE mask = 1;
        mask = mask<<remainder;
        
        if(!(hash_array[bitVecNum] & mask)){
            non_zero_count++;
            hash_array[bitVecNum] |= mask;
        }
    }
    

    inline void insert(const State& state)
    { 
        register uint index = state.hash_value() % entries;

            
        insert(index);
    }

protected:
    uint entries;

private:
    

//     uint saturation;
    
    uint array_size;

    
    static const ELEM_TYPE big = -1;
    
    ELEM_TYPE* hash_array;

    int non_zero_count;
};



/* When storing the actual state is impossible (local search is not
 * complete anyway.).*/
// typedef tr1::unordered_set<size_t, boost::hash<size_t> > SetOfStateHashes;

class SetOfStateHashes_with_Tabu : public SetOfStateHashes
{
public:
    SetOfStateHashes_with_Tabu(size_t tabu_length = 50, size_t hash_array_size = 1000000)
        :SetOfStateHashes(hash_array_size),
         tabu_length(tabu_length)
    {
        index = 0;
        tabu = new size_t[tabu_length];
        for(uint i = 0; i < tabu_length; i++){
            tabu[i] = 0;
        }
    }

    ~SetOfStateHashes_with_Tabu(){
        if(tabu)
            delete [] tabu;
    }

    SetOfStateHashes_with_Tabu(const SetOfStateHashes_with_Tabu& in)
        :SetOfStateHashes(in)
    {
        //SetOfStateHashes::(in);

        tabu_length = in.tabu_length;
        index = 0;
        tabu = new size_t[tabu_length];
        for(uint i = 0; i < tabu_length; i++){
            tabu[i] = 0;
        }
    }
    
    SetOfStateHashes_with_Tabu& operator=(const SetOfStateHashes_with_Tabu& in)
    {
        SetOfStateHashes::operator=(in);

        tabu_length = in.tabu_length;
        index = 0;
        tabu = new size_t[tabu_length];
        for(uint i = 0; i < tabu_length; i++){
            tabu[i] = 0;
        }
        return *this;
    }
    
    inline void insert(const State& state)
    { 
        register uint hash = state.hash_value() % entries;
        /*If the insertion is possible according to the tabu. */
        if(internal_find(hash) == end()){
            //std::cerr<<"State at index :: "<<hash<<" is false"<<std::endl;
            SetOfStateHashes::insert(hash);
            tabu[index] = hash;
            //std::cerr<<"Adding index :: "<<index<<" "<<tabu[static_cast<size_t>(index)]<<std::endl;
            int release_index = -1 * ((int)index - (int)tabu_length + 1);
            index = (index + 1) % tabu_length;
            assert( release_index < tabu_length );
            assert( release_index >= 0 );
            
            //std::cerr<<"Reseasing index :: "<<release_index<<" "<<tabu[static_cast<size_t>(release_index)]<<std::endl;
            SetOfStateHashes::flip(tabu[static_cast<size_t>(release_index)]);
        }
        
    }
private:
    size_t index;
    size_t* tabu;
    size_t tabu_length;
};

/*Set of states. This is the TABU.*/
//typedef tr1::unordered_set<State, boost::hash<State> > SetOfStates;
//typedef SetOfStateHashes SetOfStates;
#ifdef USE_setOfStates_withTabu
typedef SetOfStateHashes_with_Tabu SetOfStates;
#else
#ifdef USE_setOfStates
typedef SetOfStateHashes SetOfStates;
#endif
#endif


#endif
