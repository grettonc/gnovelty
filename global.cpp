/**********************************************************/
/* gnovelty.hpp, version 1.0                              */
/*                                                        */
/* Duc Nghia Pham (d.pham@griffith.edu.au)                */ 
/*   IIIS, Griffith University, Australia                 */
/* February 2005                                          */
/**********************************************************/


#include "global.hpp"

/**********************************************************/
/* General parameters                                     */
/**********************************************************/
int seed;                    // seed used for randomization
int nbRuns;                  // number of runs
int timeout;                 // maximal number of seconds per try
int target;                  // cost to reach in order to have a solution
int printSol;                // flag determining whether to output solution

double expertime;


/**********************************************************/
/* Basic variables and data structures                    */
/**********************************************************/
int nbVars;                  // number of variables in input formula
int nbCls;                   // number of clauses in input formula
int nbLits;                  // number of literals in input formula
int maxLits;

int **clauses;               // clauses -> literals : clauses[i][0]   - the size
int **occurs;                // literals -> clauses : occurs[i][0]    - the size

int **neighbours;

bool *solution;	             // where the last solution gets saved
int  totalCost;               // sum of costs associated w/ all false clauses
int  lowCost;                 // best totalCost seen so far
long lowFlips;                // number of flips it took to reach lowCost

int    finalCost;
long   finalFlips;
double finalRuntime;

int  nbThreads;
int  tabuSize;

/**********************************************************/
/* ANOV parameters                                        */
/**********************************************************/
prob_t GLOBAL__noise       = 50;
prob_t GLOBAL__smoothProb  = 0;
prob_t GLOBAL__walkProb    = 1;    // default random walk probability


/**********************************************************/
/*  Global Statistics                                     */
/**********************************************************/
long totalFlips;              // total number of flips in all runs so far
long totalNullFlips;          // total num of nullFlips in all runs so far
long totalSuccessFlips;       // total num of flips in succesful runs so far
long totalSuccessNullFlips;   // total num of null flips in succesful runs so far
int  successRuns;             // total number of successful runs so far



