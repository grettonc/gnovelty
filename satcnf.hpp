/************************************************************/
/* gnovelty+.hpp, version 1.2                               */
/*                                                          */
/*      A greedy gradient based Novelty+                    */
/*                                                          */
/*      1. Duc Nghia Pham (duc-nghia.pham@nicta.com.au)	    */ 
/*            National ICT Australia Ltd.                   */
/*            IIIS, Griffith University, Australia          */
/*                                                          */
/*      2. Charles Gretton (charles.gretton@computer.org)   */ 
/*            National ICT Australia Ltd. 2006/09; 2011/12  */
/*            University of Birmingham (2009-2011)          */
/*            IIIS, Griffith University, Australia 2006/12  */
/*                                                          */
/*                                                          */
/************************************************************/


/**
 * Filename: satcnf.hpp
 * Author:   Duc Nghia Pham
 * Created:  20th September 2010
 *
 * Provides functions to parse a CNF formula
 */

#ifndef __SATCNF_HPP__
#define __SATCNF_HPP__

bool parseCNF(char *inFile);

#endif

