/************************************************************/
/* gnovelty+.hpp, version 1.2                               */
/*                                                          */
/*      A greedy gradient based Novelty+                    */
/*                                                          */
/*      1. Duc Nghia Pham (duc-nghia.pham@nicta.com.au)	    */ 
/*            National ICT Australia Ltd.                   */
/*            IIIS, Griffith University, Australia          */
/*                                                          */
/*      2. Charles Gretton (charles.gretton@computer.org)   */ 
/*            National ICT Australia Ltd. 2006/09; 2011/12  */
/*            University of Birmingham (2009-2011)          */
/*            IIIS, Griffith University, Australia 2006/12  */
/*                                                          */
/*                                                          */
/************************************************************/

#ifndef __MY_GNOVELTY_HPP__
#define __MY_GNOVELTY_HPP__

#include "State.hpp"
#include "PartialState.hpp"

class GNoveltyPlus {
public:
	/* Charles-to-Nghia: You will want to make a new hash for every subset
	 * you are interesting in tracking. This is kind of obvious but I
	 * wanted to check that you were going to do this. We do not want to
	 * have to appeal to the mapping from propositions to elements in the
	 * bitvector while we are hashing, otherwise I believe things will
	 * become too slow. Incidentally, for the moment this is tracking
	 * states from an arbitrary set of atoms chosen in the call to
	 * \function{initInstance}.*/
	
	GNoveltyPlus(uint numAtoms,
                     uint numClauses,
                     uint juhan__hash_array_size = 1000000,
                     uint nghia__tabu_length = 50) 
		: noise(GLOBAL__noise),
		smoothProb(GLOBAL__smoothProb),
		walkProb(GLOBAL__walkProb),
		adaptive(true),
		state(nbVars+1),
		juhan__hash_array_size(juhan__hash_array_size),
                  nghia__tabu_length(nghia__tabu_length)
	{
            /* If the threading system has fallen on its face.*/
            if(0 != pthread_attr_init(&attributes)){
                std::cerr<<"Unable to allocate attributes to a gnovelty+ thread."<<std::endl;
                exit(-1);
            }
            
            if(0 != pthread_attr_setdetachstate(&attributes, PTHREAD_CREATE_DETACHED)){
                std::cerr<<"Failing in making a thread that we don't have to join..."<<std::endl;
                exit(-1);
            }
            
            if(0 != pthread_attr_setscope(&attributes, PTHREAD_SCOPE_SYSTEM)){//,//PTHREAD_SCOPE_PROCESS),
                //PTHREAD_SCOPE_SYSTEM
                std::cerr<<"Failed to give the thread a process scope..."<<std::endl;
                exit(-1);
            }
            
            if(0 != pthread_attr_setinheritsched(&attributes, PTHREAD_INHERIT_SCHED)){
                std::cerr<<"Failed to Make the thread execution schedule, that of its parent..."<<std::endl;
            }

                #ifdef USE_setOfStates_withTabu 
                setOfStates = SetOfStates(nghia__tabu_length, juhan__hash_array_size);
                #else
                #ifdef USE_setOfStates 
		setOfStates = SetOfStates(juhan__hash_array_size);
                #endif
		#endif
                
        
		candVars       = new int[nbVars + 1];
		candidates     = new bool[nbVars + 1];
		falseClauses   = new int[nbCls + 1];
		falseClsIdx    = new int[nbCls];
		clsTrueLits    = new int[nbCls];
		clsCritVars    = new int[nbCls];
		clsWeights     = new cw_t[nbCls];
		varWeightDiffs = new cw_t[nbVars + 1];
		wgtClauses     = new int[nbCls + 1];
		wgtClsIdx      = new int[nbCls];
		varAges        = new long[nbVars + 1];
    }
	
	~GNoveltyPlus() {
		delete [] candVars;
		delete [] candidates;
		delete [] falseClauses;
		delete [] falseClsIdx;
		delete [] clsTrueLits;
		delete [] clsCritVars;
		delete [] clsWeights;
		delete [] varWeightDiffs;
		delete [] wgtClauses;
		delete [] wgtClsIdx;
		delete [] varAges;
	}
    
	void updateStatsEndTry();
	void updateStatsEndFlip();
	cw_t computeCurrentCost();
	void saveSolution();
	
	void init();
	int  pickVar();
	void flipVar(int toflip);
	
	void updateClauseWeights();
	void smooth();
	void adaptNoveltyNoise();

    void addFalseClause(int c);
	void remFalseClause(int c);
	void addWeightedClause(int c);
	void remWeightedClause(int c);
	
public:
	prob_t noise;
	prob_t smoothProb;
	prob_t walkProb;

	int var;

	long flip;                  // counter for flips
	long nullFlip;              // counter for nullFlips

	int*  candVars;
	bool* candidates;
	
	int* falseClauses;          // list of false clauses
	int* falseClsIdx;           // falseClauses' index of clause ith 
	
	int* clsTrueLits;           // number of true literals in each clause
	int* clsCritVars;           // the literal for clauses critically sat
	
	//=== Weighting controlled variables
	cw_t* clsWeights;           // weight of a clause
	cw_t* varWeightDiffs;       // the total weight difference if a variable if flipped
	cw_t  totalWeight;          // the sum of false clause weights

	int* wgtClauses;            // clauses which have weight > 1:
	int* wgtClsIdx;             // same as for falseClsIdx

	//=== AdaptNovelty+ controlled variables
	long* varAges;
	long  lastAdaptFlip;
	long  lastAdaptNumFalse;
	bool  adaptive;

	State state;
#ifdef USE_setOfStates 
	SetOfStates setOfStates;
#endif
    
#ifdef USE_setOfPartialStates 
	PartialState partialState;//(vector<uint>());
	SetOfStates  setOfPartialStates;
#endif

	// Thread and thread attributes for the local search.
	pthread_t       thread;
	pthread_attr_t  attributes;

	uint juhan__hash_array_size;
        uint nghia__tabu_length;
    
	double runtime;
};

/**
 * Static functions
 */
void* runGNoveltyPlus(void* _gthread);
void  findNeighbours();

extern
pthread_mutex_t solution_mutex;

extern
pthread_mutex_t running_mutex;

extern
size_t running_count;

void increment__running_count();
void decrement__running_count();

#endif

